﻿using System;
using System.Collections.Generic;

namespace MathByGrade
{
	class G1Problems
	{
		public static List<IProblemGenerator> GetProblemGenerators()
		{
			return new List<IProblemGenerator>
			{
				new AddingTens(),
				new SimpleEquality(),
				new AddingSmallNumbers(),
				new SubtractingSmallNumbers()
			};
		}
		private class AddingSmallNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Adding numbers 0 - 20";

			public Problem GenerateProblem()
			{
				var x = rand.Next(0, 20);
				var y = rand.Next(0, 20);
				return new Problem($"{x} + {y} = ", $"{x + y}");
			}
		}

		private class SubtractingSmallNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Subtracting numbers 0 - 20";

			public Problem GenerateProblem()
			{
				var x = rand.Next(1, 20);
				var y = rand.Next(0, 19);
				if (y > x)
				{
					var z = y;
					y = x;
					x = z;
				}
				return new Problem($"{x} - {y} = ", $"{x - y}");
			}

		}

		private class SimpleEquality : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Greater than, Less than or Equal with numbers less than 20";

			public Problem GenerateProblem()
			{
				var x = rand.Next(0, 20);
				var y = rand.Next(1, 20);
				var sign = x == y ? "=" : x < y ? "<" : ">"; 
				return new Problem($"{x} __ {y} (choose >,=,<)", sign);
			}
		}

		private class AddingTens : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Adding 10s";

			public Problem GenerateProblem()
			{
				var x = rand.Next(0, 9) * 10;
				var y = rand.Next(0, 9) * 10;
				return new Problem($"{x} + {y} = ", $"{x + y}");
			}

		}
	}
}
