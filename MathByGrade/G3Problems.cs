﻿using System;
using System.Collections.Generic;

namespace MathByGrade
{
	class G3Problems
	{
		public static List<IProblemGenerator> GetProblemGenerators()
		{
			return new List<IProblemGenerator>
			{
				new TimeTables(),
				new SimpleDivision(),
				new AddingBigNumbers(),
				new SubtractingBigNumbers()
			};
		}
		private class AddingBigNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Adding 2 & 3 digit numbers with carrying";

			public Problem GenerateProblem()
			{
				var x = rand.Next(1, 999);
				var y = rand.Next(0, 998);
				return new Problem($"{x} + {y} = ", $"{x + y}");
			}
		}

		private class SubtractingBigNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();

			public string Description => "Subtracting 2 & 3 digit numbers with borrowing";

			public Problem GenerateProblem()
			{
				var x = rand.Next(1, 999);
				var y = rand.Next(0, 998);
				if (y > x)
				{
					var z = y;
					y = x;
					x = z;
				}
				return new Problem($"{x} - {y} = ", $"{x - y}");
			}

		}

		private class SimpleDivision : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Division, results upto 12";

			public Problem GenerateProblem()
			{
				var x = rand.Next(0, 12);
				var y = rand.Next(1, 12);
				return new Problem($"{x * y} ÷ {y} = ", $"{x * y}");
			}
		}

		private class TimeTables : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Times tables, 0 - 12";

			public Problem GenerateProblem()
			{
				var x = rand.Next(0, 12);
				var y = rand.Next(0, 12);
				return new Problem($"{x} X {y} = ", $"{x * y}");
			}

		}
	}
}
