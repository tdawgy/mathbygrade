﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MathByGrade
{
	class G2Problems
	{
		public static List<IProblemGenerator> GetProblemGenerators()
		{
			return new List<IProblemGenerator>
			{
				new SortNumbers(),
				new AddingBigNumbersNumbers(),
				new SubtractingSmallNumbers(),
				new Equality()
			};
		}
		private class AddingBigNumbersNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Adding 2 or 3 digit numbers, no carrying";

			public Problem GenerateProblem()
			{
				var x = rand.Next(10, 999);
				var y = rand.Next(0, 9 - x%10);
				if(x > 9) y += rand.Next(0, 9 - (x / 10) % 10);
				if(x > 99) y += rand.Next(0, 9 - (x / 100) % 10);
				return new Problem($"{x} + {y} = ", $"{x + y}");
			}
		}

		private class SubtractingSmallNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Subtracting 2 or 3 digit numbers, no borrowing";

			public Problem GenerateProblem()
			{
				var x = rand.Next(1, 20);
				var y = rand.Next(0, 9 - x % 10);
				if (x > 9) y += rand.Next(0, (x / 10) % 10) * 10;
				if (x > 99) y += rand.Next(0, (x / 100) % 10) * 100;
				return new Problem($"{x} - {y} = ", $"{x - y}");
			}

		}

		private class SortNumbers : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Sort similar 3 digit numbers";

			public Problem GenerateProblem()
			{
				var nums = new List<int>();
				var x = rand.Next(101, 999);
				nums.Add(x);
				nums.Add(rand.Next(0,99) * x / 100);
				x = rand.Next(100, 999);
				nums.Add(x);
				nums.Add(rand.Next(0,99) * x / 100);
				return new Problem($"{string.Join(", ", nums.OrderBy(n=> rand.Next(0,1000)))} = ", string.Join(", ", nums.OrderBy(n => n)));
			}
		}

		private class Equality : IProblemGenerator
		{
			private readonly Random rand = new Random();
			public string Description => "Greater than, Less than or Equal 3 digit numbers";

			public Problem GenerateProblem()
			{
				var x = rand.Next(100, 999);
				var y = rand.Next(100, 999);
				var sign = x == y ? "=" : x < y ? "<" : ">";
				return new Problem($"{x * y} ÷ {y} = ", sign);
			}
		}

	}
}
