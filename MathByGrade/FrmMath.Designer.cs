﻿namespace MathByGrade
{
	partial class FrmMath
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabGrade = new System.Windows.Forms.TabControl();
			this.tabG1 = new System.Windows.Forms.TabPage();
			this.cbG1 = new System.Windows.Forms.CheckedListBox();
			this.tabG2 = new System.Windows.Forms.TabPage();
			this.cbG2 = new System.Windows.Forms.CheckedListBox();
			this.tabG3 = new System.Windows.Forms.TabPage();
			this.cbG3 = new System.Windows.Forms.CheckedListBox();
			this.tabG4 = new System.Windows.Forms.TabPage();
			this.cbG4 = new System.Windows.Forms.CheckedListBox();
			this.probsPerCol = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.numCols = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.numProblems = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.cbAnswers = new System.Windows.Forms.CheckBox();
			this.btnPrint = new System.Windows.Forms.Button();
			this.tabGrade.SuspendLayout();
			this.tabG1.SuspendLayout();
			this.tabG2.SuspendLayout();
			this.tabG3.SuspendLayout();
			this.tabG4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.probsPerCol)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numCols)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numProblems)).BeginInit();
			this.SuspendLayout();
			// 
			// tabGrade
			// 
			this.tabGrade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabGrade.Controls.Add(this.tabG1);
			this.tabGrade.Controls.Add(this.tabG2);
			this.tabGrade.Controls.Add(this.tabG3);
			this.tabGrade.Controls.Add(this.tabG4);
			this.tabGrade.Location = new System.Drawing.Point(-4, -1);
			this.tabGrade.Name = "tabGrade";
			this.tabGrade.SelectedIndex = 0;
			this.tabGrade.Size = new System.Drawing.Size(175, 212);
			this.tabGrade.TabIndex = 0;
			this.tabGrade.SelectedIndexChanged += new System.EventHandler(this.tabGrade_SelectedIndexChanged);
			// 
			// tabG1
			// 
			this.tabG1.Controls.Add(this.cbG1);
			this.tabG1.Location = new System.Drawing.Point(4, 22);
			this.tabG1.Name = "tabG1";
			this.tabG1.Size = new System.Drawing.Size(167, 186);
			this.tabG1.TabIndex = 0;
			this.tabG1.Text = "1st";
			this.tabG1.UseVisualStyleBackColor = true;
			// 
			// cbG1
			// 
			this.cbG1.CheckOnClick = true;
			this.cbG1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbG1.FormattingEnabled = true;
			this.cbG1.Location = new System.Drawing.Point(0, 0);
			this.cbG1.Name = "cbG1";
			this.cbG1.Size = new System.Drawing.Size(167, 186);
			this.cbG1.TabIndex = 0;
			this.cbG1.SelectedValueChanged += new System.EventHandler(this.CheckedListBox_SelectedValueChanged);
			// 
			// tabG2
			// 
			this.tabG2.Controls.Add(this.cbG2);
			this.tabG2.Location = new System.Drawing.Point(4, 22);
			this.tabG2.Name = "tabG2";
			this.tabG2.Size = new System.Drawing.Size(167, 186);
			this.tabG2.TabIndex = 1;
			this.tabG2.Text = "2nd";
			this.tabG2.UseVisualStyleBackColor = true;
			// 
			// cbG2
			// 
			this.cbG2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbG2.FormattingEnabled = true;
			this.cbG2.Location = new System.Drawing.Point(0, 0);
			this.cbG2.Name = "cbG2";
			this.cbG2.Size = new System.Drawing.Size(167, 186);
			this.cbG2.TabIndex = 0;
			this.cbG2.SelectedValueChanged += new System.EventHandler(this.CheckedListBox_SelectedValueChanged);
			// 
			// tabG3
			// 
			this.tabG3.Controls.Add(this.cbG3);
			this.tabG3.Location = new System.Drawing.Point(4, 22);
			this.tabG3.Name = "tabG3";
			this.tabG3.Size = new System.Drawing.Size(167, 186);
			this.tabG3.TabIndex = 2;
			this.tabG3.Text = "3rd";
			this.tabG3.UseVisualStyleBackColor = true;
			// 
			// cbG3
			// 
			this.cbG3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbG3.FormattingEnabled = true;
			this.cbG3.Location = new System.Drawing.Point(0, 0);
			this.cbG3.Name = "cbG3";
			this.cbG3.Size = new System.Drawing.Size(167, 186);
			this.cbG3.TabIndex = 0;
			this.cbG3.SelectedValueChanged += new System.EventHandler(this.CheckedListBox_SelectedValueChanged);
			// 
			// tabG4
			// 
			this.tabG4.Controls.Add(this.cbG4);
			this.tabG4.Location = new System.Drawing.Point(4, 22);
			this.tabG4.Name = "tabG4";
			this.tabG4.Size = new System.Drawing.Size(167, 186);
			this.tabG4.TabIndex = 3;
			this.tabG4.Text = "4th";
			this.tabG4.UseVisualStyleBackColor = true;
			// 
			// cbG4
			// 
			this.cbG4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbG4.FormattingEnabled = true;
			this.cbG4.Location = new System.Drawing.Point(0, 0);
			this.cbG4.Name = "cbG4";
			this.cbG4.Size = new System.Drawing.Size(167, 186);
			this.cbG4.TabIndex = 0;
			this.cbG4.SelectedValueChanged += new System.EventHandler(this.CheckedListBox_SelectedValueChanged);
			// 
			// probsPerCol
			// 
			this.probsPerCol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.probsPerCol.Location = new System.Drawing.Point(305, 21);
			this.probsPerCol.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
			this.probsPerCol.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.probsPerCol.Name = "probsPerCol";
			this.probsPerCol.Size = new System.Drawing.Size(82, 20);
			this.probsPerCol.TabIndex = 1;
			this.probsPerCol.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(210, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(90, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Problems/Column";
			// 
			// numCols
			// 
			this.numCols.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numCols.Location = new System.Drawing.Point(305, 53);
			this.numCols.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.numCols.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCols.Name = "numCols";
			this.numCols.Size = new System.Drawing.Size(82, 20);
			this.numCols.TabIndex = 1;
			this.numCols.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(210, 57);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(77, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Columns/Page";
			// 
			// numProblems
			// 
			this.numProblems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numProblems.Location = new System.Drawing.Point(305, 86);
			this.numProblems.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
			this.numProblems.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numProblems.Name = "numProblems";
			this.numProblems.Size = new System.Drawing.Size(82, 20);
			this.numProblems.TabIndex = 1;
			this.numProblems.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(210, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(75, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Problem count";
			// 
			// cbAnswers
			// 
			this.cbAnswers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbAnswers.AutoSize = true;
			this.cbAnswers.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbAnswers.Location = new System.Drawing.Point(213, 123);
			this.cbAnswers.Name = "cbAnswers";
			this.cbAnswers.Size = new System.Drawing.Size(105, 17);
			this.cbAnswers.TabIndex = 3;
			this.cbAnswers.Text = "Answer sheet?   ";
			this.cbAnswers.UseVisualStyleBackColor = true;
			// 
			// btnPrint
			// 
			this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPrint.Location = new System.Drawing.Point(305, 157);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(75, 23);
			this.btnPrint.TabIndex = 5;
			this.btnPrint.Text = "Print";
			this.btnPrint.UseVisualStyleBackColor = true;
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// FrmMath
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(399, 211);
			this.Controls.Add(this.btnPrint);
			this.Controls.Add(this.cbAnswers);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numProblems);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numCols);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.probsPerCol);
			this.Controls.Add(this.tabGrade);
			this.MinimumSize = new System.Drawing.Size(415, 250);
			this.Name = "FrmMath";
			this.Text = "Math By Grade";
			this.tabGrade.ResumeLayout(false);
			this.tabG1.ResumeLayout(false);
			this.tabG2.ResumeLayout(false);
			this.tabG3.ResumeLayout(false);
			this.tabG4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.probsPerCol)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numCols)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numProblems)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TabControl tabGrade;
		private System.Windows.Forms.TabPage tabG1;
		private System.Windows.Forms.CheckedListBox cbG1;
		private System.Windows.Forms.CheckedListBox cbG2;
		private System.Windows.Forms.CheckedListBox cbG3;
		private System.Windows.Forms.CheckedListBox cbG4;
		private System.Windows.Forms.TabPage tabG2;
		private System.Windows.Forms.TabPage tabG3;
		private System.Windows.Forms.TabPage tabG4;
		private System.Windows.Forms.NumericUpDown probsPerCol;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numCols;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numProblems;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox cbAnswers;
		private System.Windows.Forms.Button btnPrint;
	}
}

