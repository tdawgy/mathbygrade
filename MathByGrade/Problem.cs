﻿using System.Collections.Generic;

namespace MathByGrade
{
	internal class Problem
	{
		public Problem(string text, string answer)
		{
			Answer = answer;
			Text = text;
		}

		public string Text { get; private set; }
		public string Answer { get; private set; }
	}

	interface IProblemGenerator
	{
		string Description { get; }
		Problem GenerateProblem();
	}

	internal class ProblemsByGrade
	{
		private static Dictionary<int, List<IProblemGenerator>> _probs;

		public static Dictionary<int, List<IProblemGenerator>> Problems => _probs ?? GenerateProblemsByGrade();

		private static Dictionary<int, List<IProblemGenerator>> GenerateProblemsByGrade()
		{
			_probs = new Dictionary<int, List<IProblemGenerator>>
			{
				[1] = G1Problems.GetProblemGenerators(),
				[2] = G2Problems.GetProblemGenerators(),
				[3] = G3Problems.GetProblemGenerators(),
				[4] = G4Problems.GetProblemGenerators()
			};
			return _probs;
		}
	}
}